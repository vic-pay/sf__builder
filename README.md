# SkillFactory: DEVOPS. Практикум. Модуль 22. Системы сборки

## Задание 
22.1. Клонируйте репозиторий в Gitlab https://github.com/stefanprodan/podinfo.

22.2. Добавить .gitlab-ci.yml, в котором будет произведена сборка образа, его тестирование и добавление в GitLab Container Registry.

## Решение 
Создан пайплайн решающий задачу.
Пайплан доступен по ссылке - https://gitlab.com/vic-pay/sf__builder/-/blob/master/.gitlab-ci.yml
Результаты доступны по ссылке - https://gitlab.com/vic-pay/sf__builder/-/pipelines

